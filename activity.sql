-- activity.sql
	USE `classic_models`;


	-- 1. Return the customerName of the customers who are from the Philippines
	SELECT customerName FROM customers WHERE country = 'Philippines';


	-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
	SELECT contactLastName, contactFirstName FROM customers WHERE customerName = 'La Rochelle Gifts';
		-- for double checking
		SELECT contactLastName, contactFirstName, customerName FROM customers WHERE customerName = 'La Rochelle Gifts';




	-- 3. Return the product name and MSRP of the product named "The Titanic"
	SELECT productName, MSRP FROM products WHERE productName = 'The Titanic';



	-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
	SELECT firstName, lastName FROM employees WHERE email = 'jfirrelli@classicmodelcars.com';
		-- there are 2 results so in this case better to ask for other identifying details such as first name to pull up one result only EXAMPLE:
		SELECT firstName, lastName FROM employees WHERE email = 'jfirrelli@classicmodelcars.com' AND firstName = "Jeff";
		SELECT firstName, lastName FROM employees WHERE email = 'jfirrelli@classicmodelcars.com' AND firstName = "Julie";


	-- 5. Return the names of customers who have no registered state
	SELECT customerName FROM customers WHERE state IS NULL OR state = '';


	-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
	SELECT firstName, lastName, email FROM employees WHERE firstName = "Steve" AND lastName = "Patterson";



	-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
	-- for double checking only
	SELECT creditLimit FROM customers;
	-- answer:
	SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000.00;
	

	-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'
	-- answer
	SELECT customers.customerNumber FROM customers 
		JOIN orders ON customers.customerNumber = orders.customerNumber
		WHERE orders.comments LIKE "%DHL%";
	-- order results in ascending order by number
	SELECT customers.customerNumber FROM customers 
		JOIN orders ON customers.customerNumber = orders.customerNumber
		WHERE orders.comments LIKE "%DHL%"
		ORDER BY customers.customerNumber ASC;




	-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
	SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";




	-- 10. Return the countries of customers without duplication
	-- answer
	SELECT DISTINCT country FROM customers;
	-- order results in alphabetical order
	SELECT DISTINCT country FROM customers
		ORDER BY country ASC;


	-- 11. Return the statuses of orders without duplication
	SELECT DISTINCT status FROM orders;


	-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
	-- answer
	SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");
	-- group results by country
	SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada") ORDER BY country ASC;


	-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
	SELECT employees.firstName, employees.lastName, offices.city FROM employees
		JOIN offices ON employees.officeCode = offices.officeCode
		WHERE offices.city = "Tokyo";



	-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
	-- answer 
	SELECT customerName FROM customers
		JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
		WHERE employees.lastName = "Thompson" AND employees.firstName = "Leslie";

		-- for double checking only
		-- getting Leslie Thompson's employee number
		SELECT employeeNumber FROM employees WHERE lastName = "Thompson" AND firstName = "Leslie";
		-- showing a table with salesRepEmployeeNumber
		SELECT customerName, salesRepEmployeeNumber FROM customers
			JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
			WHERE employees.lastName = "Thompson" AND employees.firstName = "Leslie";


	-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"
	SELECT products.productName, customers.customerName FROM customers
		JOIN orders ON orders.customerNumber = customers.customerNumber
		JOIN orderdetails ON orderdetails.orderNumber = orders.orderNumber
		JOIN products ON products.productCode = orderdetails.productCode
		WHERE customers.customerName = "Baane Mini Imports";



	-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
	SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
		JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
		JOIN offices ON employees.officeCode = offices.officeCode
		WHERE customers.country = offices.country;

		-- added customers.country on SELECT to double check if they are indeed the same
		SELECT employees.firstName, employees.lastName, customers.customerName, customers.country, offices.country FROM customers
			JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
			JOIN offices ON employees.officeCode = offices.officeCode
			WHERE customers.country = offices.country;



	-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
	SELECT products.productName, products.quantityInStock FROM products
		JOIN productlines ON products.productLine = productlines.productLine
		WHERE products.productLine = "planes" AND products.quantityInStock < 1000;

		-- for double checking
		SELECT products.productName, products.quantityInStock , productlines.productLine FROM products
			JOIN productlines ON products.productLine = productlines.productLine
			WHERE products.productLine = "planes" AND products.quantityInStock < 1000;

	-- 18. Show the customer's name with a phone number containing "+81".
	SELECT customerName FROM customers WHERE phone LIKE "%+81%";

		-- additinal code to know where the country this customer is from
		SELECT customerName, country FROM customers WHERE phone LIKE "%+81%";


/*DONE!*/
